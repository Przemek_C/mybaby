package com.ciaston.przemek.mybaby.db;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;

import java.text.SimpleDateFormat;

/**
 * Created by Przemek on 2017-08-31.
 */

public class DBHelper extends SQLiteOpenHelper {

    public static final int VERSION = 1;

    public static final String DB_NAME = "MyBaby.db";
    public static final String TABLE_NAME = "One_day";
    public static final String THEME_COLUMN = "theme";
    public static final String DATE_COLUMN = "date_added";
    public static final String DESCRIPTION_COLUMN = "description";
    public static final String PHOTO_PATH_COLUMN = "photo";
    private SQLiteDatabase db;

    String createTable = "CREATE TABLE IF NOT EXISTS " + TABLE_NAME +
            "(ID INTEGER PRIMARY KEY AUTOINCREMENT, " +
            THEME_COLUMN + " TEXT, " +
            DATE_COLUMN + " TEXT, " +
            DESCRIPTION_COLUMN + " TEXT, " +
            PHOTO_PATH_COLUMN + " TEXT);";

    String deleteTable = "DROP IF EXISTS " + DB_NAME;

    public DBHelper(Context context) {
        super(context, DB_NAME, null, VERSION);
    }

    @Override
    public void onCreate(SQLiteDatabase db) {
        db.execSQL(createTable);
    }

    @Override
    public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
        db.execSQL(deleteTable);
    }

    public boolean addData(String theme, String description, String photoPath) {
        db = this.getWritableDatabase();
        ContentValues addToDB = new ContentValues();
        addToDB.put(THEME_COLUMN, theme);
        addToDB.put(DATE_COLUMN, actualDate());
        addToDB.put(DESCRIPTION_COLUMN, description);
        addToDB.put(PHOTO_PATH_COLUMN, photoPath);

        long result = db.insert(TABLE_NAME, null, addToDB);

        if (result == -1) {
            return false;
        } else {
            return true;
        }
    }

    public Cursor getList(){
        db = this.getReadableDatabase();
        String select = "SELECT * FROM " + TABLE_NAME;
        Cursor cursor = db.rawQuery(select, null);
        return cursor;
    }

    public Cursor getId(String theme){
        db = this.getReadableDatabase();
        String select = "SELECT id FROM " + TABLE_NAME + " WHERE theme LIKE " + theme;
        Cursor cursor = db.rawQuery(select, null);
        return cursor;
    }

    public void deleteFromDB(int id){
        db = this.getReadableDatabase();
        String whereClause = "id = ?";
        String[] arg = {String.valueOf(id)};
        db.delete(TABLE_NAME, whereClause, arg);
    }

    public void closeDB() {
        db.close();
    }

    public String actualDate() {
        SimpleDateFormat simpleDateFormat = new SimpleDateFormat("yyyy-MM-dd HH:mm");
        String actualDate = simpleDateFormat.format(System.currentTimeMillis());
        return actualDate;
    }
}
