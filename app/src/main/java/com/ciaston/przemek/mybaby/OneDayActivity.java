package com.ciaston.przemek.mybaby;

import android.Manifest;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.database.Cursor;
import android.graphics.Bitmap;
import android.net.Uri;
import android.os.Environment;
import android.provider.MediaStore;
import android.support.annotation.NonNull;
import android.support.v4.app.ActivityCompat;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.Toast;


import com.ciaston.przemek.mybaby.db.DBHelper;
import com.ciaston.przemek.mybaby.model.SmsActivity;

import java.io.File;
import java.io.FileOutputStream;
import java.text.SimpleDateFormat;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

public class OneDayActivity extends AppCompatActivity {

    public static final int CAMERA_REQUEST = 1;
    public static final int WRITE_EXTERNAL_STORAGE = 2;

    public static final String SMS_MESSAGE = "wiadomość SMS";

    @BindView(R.id.themeEditText) EditText themeEditText;
 //   @BindView(R.id.dateEditText) EditText dateEditText;
    @BindView(R.id.describeDayEditText) EditText describeDayEditText;
    @BindView(R.id.babyImageView) ImageView babyImageView;
    Bitmap photo;
    DBHelper dbHelper;

    @OnClick(R.id.babyImageView)
    public void intentToCamera() {
        askPermission(Manifest.permission.CAMERA, CAMERA_REQUEST);
        askPermission(Manifest.permission.WRITE_EXTERNAL_STORAGE, WRITE_EXTERNAL_STORAGE);
    }

    private void cameraStart() {
        Intent intent = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);
        startActivityForResult(intent, CAMERA_REQUEST);
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_one_day);
        ButterKnife.bind(this);


    }

    public void askPermission(String permission, int requestCode) {
        if (ContextCompat.checkSelfPermission(this, permission) != PackageManager.PERMISSION_GRANTED) {
            ActivityCompat.requestPermissions(this, new String[]{permission}, requestCode);
        } else {
            cameraStart();
//            Toast.makeText(getApplicationContext(), "Permission is alredy granted", Toast.LENGTH_LONG).show();
        }
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        switch (requestCode) {
            case CAMERA_REQUEST:
                if (grantResults.length > 0 && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                    cameraStart();
                } else {
                    Toast.makeText(getApplicationContext(), "Permission Denied", Toast.LENGTH_LONG).show();
                }
            case WRITE_EXTERNAL_STORAGE:
                if (grantResults.length > 0 && grantResults[0] == PackageManager.PERMISSION_GRANTED) {

                } else {
                    Toast.makeText(getApplicationContext(), "Permission Denied", Toast.LENGTH_LONG).show();
                }
        }
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        MenuInflater inflater = getMenuInflater();
        inflater.inflate(R.menu.menu_item, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        int id = item.getItemId();
        switch (id) {
            case R.id.add_item:
                addToActivityList();
                return true;
            case R.id.email_item:
                sendEmail();
                return true;
            case R.id.sms_item:
                goToSmsActivity();
                return true;
        }
        return super.onOptionsItemSelected(item);
    }

    private void goToSmsActivity() {
        String string = new String();
        string = "Temat: " + themeEditText.getText().toString() + "," + "\n\n" + describeDayEditText.getText().toString();
        Intent smsIntent = new Intent(OneDayActivity.this, SmsActivity.class);
        smsIntent.putExtra(SMS_MESSAGE, string);
        startActivity(smsIntent);

    }

    private void sendEmail() {

    }

    private void addToActivityList() {
        String theme = themeEditText.getText().toString();
        String desc = describeDayEditText.getText().toString();
        String photoPath = "Photo path";
        addData(theme, desc, photoPath);
        Intent listActivity = new Intent(OneDayActivity.this, ListActivity.class);
        startActivity(listActivity);
    }

    private void addData(String theme, String desc, String photoPath) {
        dbHelper = new DBHelper(this);
        boolean result = dbHelper.addData(theme, desc, photoPath);

        if (result == true){
            showToast("Successfully Entered Data!");
        } else {
            showToast("Something went wrong :/");
        }
        dbHelper.closeDB();
    }

    private void showToast(String message) {
        Toast.makeText(getApplicationContext(), message, Toast.LENGTH_LONG).show();
    }

    private void makePhoto(Intent data, ImageView imageView) {
        photo = (Bitmap) data.getExtras().get("data");
        Bundle extras = data.getExtras();
        photo = (Bitmap) extras.get("data");
        imageView.setAdjustViewBounds(true);
        imageView.setImageBitmap(photo);
        savePhotoToSDCard(photo);
    }

    private void savePhotoToSDCard(Bitmap bitmap) {
        File fileName;
        try {
            SimpleDateFormat simpleDateFormat = new SimpleDateFormat("yyyy-MM-dd_HH:mm:ss");
            String photoName = simpleDateFormat.format(System.currentTimeMillis());
            String path = Environment.getExternalStorageDirectory().toString();

            new File(path + "/MyBaby/").mkdirs();
            fileName = new File(path + "/MyBaby/" + photoName + ".jpeg");

            FileOutputStream fileOutputStream = new FileOutputStream(fileName);

            bitmap.compress(Bitmap.CompressFormat.JPEG, 150, fileOutputStream);
            fileOutputStream.flush();
            fileOutputStream.close();
            MediaStore.Images.Media.insertImage(getContentResolver(), fileName.getAbsolutePath(), fileName.getName(), fileName.getName());
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        if (requestCode == CAMERA_REQUEST && resultCode == RESULT_OK && data != null) {
            makePhoto(data, babyImageView);
        }
    }
}