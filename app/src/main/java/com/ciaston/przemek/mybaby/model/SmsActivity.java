package com.ciaston.przemek.mybaby.model;

import android.Manifest;
import android.content.ContentResolver;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.database.Cursor;
import android.net.Uri;
import android.os.Build;
import android.provider.ContactsContract;
import android.support.annotation.NonNull;
import android.support.v4.app.ActivityCompat;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.telephony.SmsManager;
import android.view.View;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.Toast;

import com.ciaston.przemek.mybaby.R;

import static com.ciaston.przemek.mybaby.OneDayActivity.SMS_MESSAGE;

public class SmsActivity extends AppCompatActivity {

    private static final int REQUEST_CODE_PICK_CONTACTS = 1;
    private static final int PERMISSION_REQUEST_CODE = 1;
    EditText phoneNumber;
    EditText smsMessage;
    ImageButton sendSMS;
    ImageButton imageButton;
    private Uri uriContact;
    private String contactID;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_sms);

        phoneNumber = (EditText) findViewById(R.id.phone_number);
        smsMessage = (EditText) findViewById(R.id.sms_message);
        imageButton = (ImageButton) findViewById(R.id.imageButton);

        Bundle bundle = getIntent().getExtras();
        smsMessage.setText(bundle.getString(SMS_MESSAGE));

        sendSMS = (ImageButton) findViewById(R.id.send_sms);
        sendSMS.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                sendMessage();
            }
        });

        imageButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                //showContants();
                if (Build.VERSION.SDK_INT >= 23) {
                    if (checkPermission()) {
                        Toast.makeText(getApplicationContext(), "Permission GRANTED!", Toast.LENGTH_LONG).show();
                        showContants();
                    } else {
                        requestPermissions();
                    }
                }
            }
        });
    }

    private boolean checkPermission() {
        int result = ContextCompat.checkSelfPermission(SmsActivity.this, Manifest.permission.READ_CONTACTS);
        if (result == PackageManager.PERMISSION_GRANTED) {
            return true;
        } else {
            return false;
        }
    }

    private void requestPermissions() {
        ActivityCompat.requestPermissions(this, new String[]{Manifest.permission.READ_CONTACTS}, PERMISSION_REQUEST_CODE);
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        switch (requestCode) {
            case PERMISSION_REQUEST_CODE:
                if (grantResults.length > 0 && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                    Toast.makeText(getApplicationContext(), "Permission GRANTED", Toast.LENGTH_LONG).show();
                    showContants();
                } else {
                    Toast.makeText(getApplicationContext(), "Permission denied", Toast.LENGTH_LONG).show();
                }
                break;
        }
    }

    private void sendMessage() {
        String phone = phoneNumber.getText().toString();
        String text = smsMessage.getText().toString();
        try {
            SmsManager smsManager = SmsManager.getDefault();
            smsManager.sendTextMessage(phone, null, text, null, null);
            Toast.makeText(getApplicationContext(), "SMS został wysłany!", Toast.LENGTH_LONG).show();
        } catch (Exception e) {
            Toast.makeText(getApplicationContext(), "SMS faild, please try again later!", Toast.LENGTH_LONG).show();
            e.printStackTrace();
        }

    }

    private void showContants() {
        Intent startPickPhone = new Intent(Intent.ACTION_PICK, ContactsContract.CommonDataKinds.Phone.CONTENT_URI);
        startActivityForResult(startPickPhone, REQUEST_CODE_PICK_CONTACTS);
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        if (resultCode == RESULT_OK) {
            switch (requestCode) {
                case REQUEST_CODE_PICK_CONTACTS:
                    pickPhoneNumber(data);
                    break;
            }
        }
    }

    private void pickPhoneNumber(Intent data) {
        Cursor cursor = null;
        try {
            String phoneNumber = null;
            // getData() method will have the Content Uri of the selected contact
            Uri uri = data.getData();
            cursor = getContentResolver().query(uri, null, null, null, null);
            cursor.moveToFirst();
            // column index of the phone number
            int phoneIndex = cursor.getColumnIndex(ContactsContract.CommonDataKinds.Phone.NUMBER);
            phoneNumber = cursor.getString(phoneIndex);
            this.phoneNumber.setText(phoneNumber);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }
}

