package com.ciaston.przemek.mybaby.model;

/**
 * Created by Przemek on 2017-09-02.
 */

public class OneDay {
    private int id;
    private String theme;
    private String date;
    private String description;

    public OneDay( String theme, String date, String description) {
        this.theme = theme;
        this.date = date;
        this.description = description;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getTheme() {
        return theme;
    }

    public void setTheme(String theme) {
        this.theme = theme;
    }

    public String getDate() {
        return date;
    }

    public void setDate(String date) {
        this.date = date;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }
}
