package com.ciaston.przemek.mybaby;

import android.content.ContentResolver;
import android.content.Intent;
import android.database.CharArrayBuffer;
import android.database.ContentObserver;
import android.database.Cursor;
import android.database.DataSetObserver;
import android.net.Uri;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.AdapterView;
import android.widget.EditText;
import android.widget.ListView;
import android.widget.Toast;

import com.ciaston.przemek.mybaby.adapter.OneDayAdapter;
import com.ciaston.przemek.mybaby.db.DBHelper;
import com.ciaston.przemek.mybaby.model.OneDay;

import java.util.ArrayList;
import java.util.List;

import butterknife.ButterKnife;
import butterknife.OnClick;

public class ListActivity extends AppCompatActivity {

    OneDayAdapter oneDayAdapter = null;
    Cursor cursor;
    List<OneDay> oneDayList;
    DBHelper dbHelper;
    ListView listView;
    EditText text;

    @OnClick(R.id.floatingActionButton)
    public void createNewDay() {
        Intent intent = new Intent(ListActivity.this, OneDayActivity.class);
        startActivity(intent);
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_list);
        ButterKnife.bind(this);
        showList();

        listView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                showToast("Position clicked " + oneDayList.get(position).getTheme());

                showList();

            }
        });

    }

    public void showList() {
        dbHelper = new DBHelper(this);
        listView = (ListView) findViewById(R.id.mainListView);
        oneDayList = new ArrayList<>();
        oneDayAdapter = new OneDayAdapter(this, oneDayList);
        listView.setAdapter(oneDayAdapter);

        cursor = dbHelper.getList();
        oneDayList.clear();
        while (cursor.moveToNext()) {
            String id = cursor.getString(0);
            String theme = String.valueOf(cursor.getString(1));
            if (theme.length() > 20){
                String shortTheme = theme.substring(1,20) + "...";
                theme = shortTheme;
                }
            String date = String.valueOf(cursor.getString(2));
            String description = String.valueOf(cursor.getString(3));
            if (description.length() > 80){
                String shortDesc = description.substring(1,80) + "...";
                description = shortDesc;
                }
            oneDayList.add(new OneDay(theme, date, description));

        }
        oneDayAdapter.notifyDataSetChanged();
    }


    private void showToast(String message) {
        Toast.makeText(getApplicationContext(), message, Toast.LENGTH_LONG).show();
    }

}
