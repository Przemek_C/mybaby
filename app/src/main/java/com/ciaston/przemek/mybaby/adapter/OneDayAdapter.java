package com.ciaston.przemek.mybaby.adapter;

import com.ciaston.przemek.mybaby.R;
import com.ciaston.przemek.mybaby.model.OneDay;

import android.app.Activity;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.TextView;
import java.util.List;

/**
 * Created by Przemek on 2017-09-02.
 */

public class OneDayAdapter extends ArrayAdapter<OneDay> {
    private Activity activity;
    private List<OneDay> oneDayList;

    public OneDayAdapter(Activity activity, @NonNull List<OneDay> oneDayList) {
        super(activity, R.layout.list_row, oneDayList);
        this.activity = activity;
        this.oneDayList = oneDayList;
    }

    @NonNull
    @Override
    public View getView(int position, @Nullable View convertView, @NonNull ViewGroup parent) {
        ViewHolder viewHolder;
        View rowView = convertView;
        if (rowView == null) {
            LayoutInflater layoutInflater = activity.getLayoutInflater();
            rowView = layoutInflater.inflate(R.layout.list_row, null, true);
            viewHolder = new ViewHolder();
            viewHolder.theme = (TextView) rowView.findViewById(R.id.textViewTheme);
            viewHolder.date = (TextView) rowView.findViewById(R.id.textViewDate);
            viewHolder.description = (TextView) rowView.findViewById(R.id.textViewDescribe);

            rowView.setTag(viewHolder);
        } else {
            viewHolder = (ViewHolder) rowView.getTag();
        }
        OneDay oneDay = oneDayList.get(position);
        viewHolder.theme.setText(oneDay.getTheme());
        viewHolder.date.setText(oneDay.getDate());
        viewHolder.description.setText(oneDay.getDescription());
        return rowView;
    }

    static class ViewHolder {
        public TextView theme;
        public TextView date;
        public TextView description;
//        public TextView photoPath;
    }
}
