package com.example;

import org.greenrobot.greendao.generator.DaoGenerator;
import org.greenrobot.greendao.generator.Entity;
import org.greenrobot.greendao.generator.Schema;

public class MyGreenDao {

    public static final int VERSION = 1;

    public static void main(String[] args) {

        Schema schema = new Schema(VERSION, "com.ciaston.przemek.mybaby.db");
        schema.enableKeepSectionsByDefault();

        addTables(schema);

        try {
            new DaoGenerator().generateAll(schema, "./app/src/main/java");
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    private static void addTables(Schema schema) {

        // OneDayActivity
        Entity dayDetails = schema.addEntity("DayDetails");
        dayDetails.addIdProperty().primaryKey().autoincrement();
        dayDetails.addStringProperty("theme");
        dayDetails.addStringProperty("date");
        dayDetails.addStringProperty("describe");
        dayDetails.addStringProperty("image_path");

        // ListActivity
        Entity myChild = schema.addEntity("MyChild");
        myChild.addIdProperty().primaryKey().autoincrement();
        myChild.addStringProperty("name");
        myChild.addStringProperty("birthday");
        myChild.addStringProperty("image_path");
//        myChild.addStringProperty("weight"); // waga
//        myChild.addStringProperty("height"); // wzrost

    }
}